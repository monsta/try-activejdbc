package tw.monsta.tryMemSQL;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

/**
 * tw.monsta.tryMemSQL.User description
 *
 * @author johnson
 * @since 2015-06-16
 */
@Table("users")
@IdName("uid")
public class User extends Model {
    static {
        validatePresenceOf("uname", "upsw");
    }
}
