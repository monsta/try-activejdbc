package tw.monsta.tryMemSQL;

import org.javalite.activejdbc.Base;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.stream.IntStream;

/**
 * tw.monsta.tryMemSQL.MemSQLTest description
 *
 * @author johnson
 * @since 2015-06-16
 */
public class MemSQLTest {

    public static final String JDBC_URL = "jdbc:mysql://33.33.33.33/lottery";
    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static final String JDBC_USER = "root";
    public static final String JDBC_PSW = "password";

    @Before
    public void setUp() throws URISyntaxException, IOException, SQLException {
        Base.open(JDBC_DRIVER,
            JDBC_URL,
            JDBC_USER,
            JDBC_PSW);
    }

    @After
    public void tearDown() {
        Base.close();
    }

    @Test
    public void mainTest() throws InterruptedException {
        System.out.println(LocalDateTime.now());
        IntStream.range(0, 10)
//            .parallel()   //平行下去activeJDBC就掛了，因為Base.open只支援current(single) thread
            .forEach(val -> {
                User newUser = new User();
                newUser.set("uname", "Johnson" + val);
                newUser.set("upsw", "12345abcd");
                newUser.save();
            });

        System.out.println(LocalDateTime.now());
    }
}
